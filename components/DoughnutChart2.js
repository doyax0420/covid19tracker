import { Doughnut } from 'react-chartjs-2';

export default function DoughnutChart2 ({ topTenCountryNames, topTenCases}) {
    console.log(topTenCountryNames)
    console.log(topTenCases)
    return(
        <Doughnut 
            data = {
                {
                    datasets: [
                        {
                            data:[ topTenCases ],

                            backgroundColor: [ "red", "black", "green", "yellow", "orange", "blue", "pink", "violet", "cyan", "indigo" ]
                        }
                    ],
                    labels:[ topTenCountryNames ]
                }
            }
            redraw = { false }        
        />
    )
}