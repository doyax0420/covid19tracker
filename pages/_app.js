import React from 'react'
import { Container } from 'react-bootstrap';
import NavBar from '../components/NavBar'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {
  return(
    <React.Fragment>
    <NavBar />
    <Container>
      <Component {...pageProps} />
    </Container>
  </React.Fragment>
  )
}

export default MyApp
