import React from 'react';
import Head from 'next/head'

import { Jumbotron } from 'react-bootstrap';
import toNum from '../helpers/toNum'
export default function Home({globalTotal}) {
  
  console.log(globalTotal)

  return (
    <React.Fragment>
      <Jumbotron>
        <h1>Total Covid-19 cases in the world: <strong>{globalTotal.cases}</strong></h1>
      </Jumbotron>
    </React.Fragment>
  )
}

export async function getServerSideProps(){
  //fetch data from the api endpoint
  const rest = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await rest.json()
  const countriesStats = data.countries_stat

  let total = 0;
  countriesStats.forEach(country => {
    //Steps:
    //Access the cases property of each country
    //Use toNum to convert the cases string into a number
    //Add that number to out total
    total += toNum(country.cases)
    //When done, display that total in the Jumbotron    
  })

    const globalTotal = {
      cases: total
    }

    console.log(globalTotal)
  return {
    props: {
      globalTotal
    }
  }
  
}