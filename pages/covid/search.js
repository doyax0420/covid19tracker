import React, { useState, } from 'react';
import { Form, Button, Alert } from 'react-bootstrap';
import toNum from '../../helpers/toNum';
import DoughnutChart from '../../components/DoughnutChart';


export default function Search({data}){
	
	console.log(data);

	const [targetCountry, setTargetCountry] = useState('')
	const [name, setName] = useState("");
	const [criticals, setCriticals] = useState(0);
	const [deaths, setDeaths] = useState(0);
	const [recoveries, setRecoveries] = useState(0)
	const countriesStats = data.countries_stat;

	function searchCountry(e){
		
		e.preventDefault();

		const match = countriesStats.find(country => country.country_name.toLowerCase() === targetCountry.toLowerCase())
		console.log(match)

		if(match) {
			setName(match.country_name);
			setCriticals(toNum(match.serious_critical));
			setDeaths(toNum(match.deaths))
			setRecoveries(toNum(match.total_recovered))

		}else {
			setName('');
			setCriticals(0);
			setDeaths(0);
			setRecoveries(0);
		}
		
	}

	return(
		<React.Fragment>
			<Form onSubmit={e => searchCountry(e)}>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Search for country"
						value={targetCountry}
						onChange={e => setTargetCountry(e.target.value)}
					/>
					<Form.Text className="text-muted">
						Get Covid-19 stats of searched country
					</Form.Text>

					<Button variant="primary" type="submit">
						Submit
					</Button>
				</Form.Group>
			</Form>
			
			{ name !== ''
				? 
					<React.Fragment>
						<h1>Country: {name}</h1>
						<DoughnutChart 
							criticals = { criticals }
							deaths = { deaths }
							recoveries = { recoveries }
						/>
					</React.Fragment>
					: <Alert variant="info" className="mt-4">
							Search for a country to visualize its data.
					</Alert>
			}

		</React.Fragment>
	)
}

export async function getStaticProps(){

	const rest = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await rest.json()

  return {
  	props: {
  		data
  	}
  }
}