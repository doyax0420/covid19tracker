import React from 'react';
import Banner from '../../../components/Banner';
import DoughnutChart from '../../../components/DoughnutChart';
import toNum from '../../../helpers/toNum';

export default function Country({country}){

    return(
      <React.Fragment>
        <Banner 
          country = {country.country_name}
          criticals = {country.serious_critical}
          deaths = {country.deaths}
          recoveries = {country.total_recovered}
        />
        <DoughnutChart 
          criticals={toNum(country.serious_critical)}
          deaths={toNum(country.deaths)}
          recoveries={toNum(country.total_recovered)}
        />
      </React.Fragment>
    )
}

export async function getStaticPaths(){

	const rest = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await rest.json()

  const paths = data.countries_stat.map(country => ({
      params: { id: country.country_name }
  }))

  return { paths, fallback: false }
}

export async function getStaticProps({ params }){

	const rest = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
      "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await rest.json()
  const country = data.countries_stat.find(country => country.country_name === params.id)

  return {
  	props: {
  		country
  	}
  }
}