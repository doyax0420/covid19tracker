import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import toNum from '../../helpers/toNum';

export default function Top({data}) {

    const countriesStats = data.countries_stat;
    console.log(countriesStats);

    // Only get the name and cases of each country in our array
    const countriesCases = countriesStats.map(countryStat => {
        return {
            name: countryStat.country_name,
            // Convert string to number
            cases: toNum(countryStat.cases)
        }
    })

    console.log(countriesCases);

    // Sort in descending order of total number of cases
    countriesCases.sort((a, b) => {
        if(a.cases < b.cases){
            return 1
        }else if(a.cases > b.cases){
            return -1
        }else{
            return 0
        }
    })

    console.log(countriesCases);

    return (
        <React.Fragment>
            <h1>10 Countries With The Highest Number of Cases</h1>
            <Doughnut data={{
                datasets: [{
                    data: [countriesCases[0].cases, countriesCases[1].cases, countriesCases[2].cases, countriesCases[3].cases, countriesCases[4].cases, countriesCases[5].cases, countriesCases[6].cases, countriesCases[7].cases, countriesCases[8].cases, countriesCases[9].cases],
                    backgroundColor: ["#696969", "#bada55", "#7fe5f0", "#ff0000", "#ff80ed", "#407294", "#cbcba9", "#420420", "#133337", "#065535"]
                }],
                labels: [countriesCases[0].name, countriesCases[1].name, countriesCases[2].name, countriesCases[3].name, countriesCases[4].name, countriesCases[5].name, countriesCases[6].name, countriesCases[7].name, countriesCases[8].name, countriesCases[9].name]
            }} redraw={ false }/>
        </React.Fragment>
    )
}

export async function getStaticProps() {
    //fetch data from the /courses API endpoint    
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }
    })
    const data = await res.json()
  
    //return the props
    return { 
        props: {
            data
        }
    }
}
