export default function toNum(str){
    //Convert string to array to gain access to array methods

    const arr = [...str]

    //filterout commas in the string
    const filteredArr = arr.filter(element => element !== ",")

    //Reduce the filter array back to a single string w/o commas
    //This string can now be parsed into a number via parseInt()

    return parseInt(filteredArr.reduce((x, y) => x + y))

}